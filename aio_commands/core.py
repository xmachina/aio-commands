

class Commands:
    __doc__ = """
        --- Base command Docs ---
    """
    subclasses = []

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.subclasses.append(cls)

    def __init__(self, *args, **kwargs):
        pass

    async def execute(self):
        pass
