# """
# Manager for run commands
# """
# import argparse
# import logging.config
# import inspect
#
# import catalogue.commands as commands
# from catalogue import loop
# from catalogue import settings
# from catalogue.model import get_engine
#
# logging.config.dictConfig(settings.log_settings)
# log = logging.getLogger("Manager")
#
#
# # Input arguments
# parser = argparse.ArgumentParser(description="Catalogue service commands")
# parser.add_argument('command', type=str, nargs='*', help='Command name')
# parser.add_argument('-l','--list', action='store_true')
# parser.add_argument('-d','--doc', action='store_true', help='Get command help information, use: manager <command> -d [--doc]')
#
#
# ENGINE = get_engine()
#
#
# def info_log(*txt):
#     """
#     print green text
#     """
#     print("\033[92m"+ " ".join(txt) + "\033[0m")
#
#
# def main_log(*txt):
#     """
#     print pink text
#     """
#     print('\033[1m\033[95m' + " ".join(txt) + '\033[0m \033[0m')
#
#
# def fail_log(*txt):
#     """
#     print red text
#     """
#     print("\033[91m" + " ".join(txt) +"\033[0m")
#
#
# def get_command_list():
#     """
#     Print command list
#     """
#     info_log("Available command list: ")
#
#     func_dict = {}
#
#     for name, _func in commands.__dict__.items():
#         if name.startswith("do_"):
#             name = inspect.getmodule(_func).__name__.replace('catalogue.commands.', '')
#             n = name.split(".")
#
#             if len(n) > 1:
#                 v = func_dict.setdefault(".".join(n[:-1]), [])
#                 v.append(n[-1])
#             else:
#                 base = func_dict.setdefault("~", [])
#                 base.append(name)
#
#     for k, v in func_dict.items():
#         main_log("[" + k + "]")
#         for i in v:
#             main_log("\t*", i)
#         print("")
#
#
# def get_command_doc(cmd):
#     """
#     Print command docstrings
#     """
#     try:
#         f = getattr(commands, 'do_' + cmd)
#     except AttributeError:
#         fail_log(f"\n\tInvalid command name '{cmd}'\n")
#         get_command_list()
#     else:
#         main_log("*", cmd)
#         info_log(f.__doc__)
#
#
# if __name__ == "__main__":
#     args = parser.parse_args()
#
#     if args.doc and not args.command:
#         fail_log("\n\tInvalid command, please use: manager <command> -d [--doc]")
#         get_command_list()
#
#     elif args.list or (not args.command and not args.list):
#         get_command_list()
#
#     if args.command:
#         func_name = args.command[0]
#
#         if args.doc and args.command:
#             get_command_doc(func_name)
#         else:
#
#             func_name = args.command[0]
#
#             ags = args.command[1:]
#
#             try:
#                 func = getattr(commands, 'do_' + func_name)
#             except AttributeError:
#                 fail_log("\n\tInvalid command: ", func_name, "\n")
#                 get_command_list()
#             else:
#                 log.info(f"Run command: {func_name}")
#                 loop.loop.run_until_complete(func(ENGINE, *ags))

